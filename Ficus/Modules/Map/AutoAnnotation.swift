//
//  AutoAnnotation.swift
//  Ficus
//
//  Created by MacBook on 08/06/2019.
//  Copyright © 2019 Andrew. All rights reserved.
//

import UIKit
import MapKit

class AutoAnnotation: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    
    init(coordinate: CLLocationCoordinate2D) {
        self.coordinate = coordinate
    }
}
