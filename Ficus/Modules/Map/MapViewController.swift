//
//  MapViewController.swift
//  Ficus
//
//  Created by MacBook on 08/06/2019.
//  Copyright © 2019 Andrew. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {
    
    private var model: IncludeStructure?
    
    private var overlays: [MKOverlay] = []
    
    private var coordinates: [CLLocationCoordinate2D] = [CLLocationCoordinate2DMake(50, 10),CLLocationCoordinate2DMake(52, 9),CLLocationCoordinate2DMake(53, 20),CLLocationCoordinate2DMake(56, 14)]
    
    private lazy var locationManager: CLLocationManager = {
        let manager = CLLocationManager()
        manager.delegate = self
        mapView.showsCompass = false
        mapView.showsScale = false
        mapView.showsUserLocation = true
        mapView.register(AutoAnnotationView.self, forAnnotationViewWithReuseIdentifier: AutoAnnotationView.reuseID)
        return manager
    }()
    
    private lazy var mapView: MKMapView = {
        let mapView = MKMapView()
        mapView.delegate = self
        mapView.isRotateEnabled = false
        return mapView
    }()
    
    private lazy var btnCurrentLocation: MKUserTrackingButton = {
        let btn = MKUserTrackingButton(mapView: mapView)
        btn.layer.backgroundColor = UIColor(white: 1, alpha: 0.8).cgColor
        btn.layer.borderColor = UIColor.white.cgColor
        btn.layer.borderWidth = 1
        btn.layer.cornerRadius = 5
        return btn
    }()
    
    override func loadView() {
        super.loadView()
        
        view = UIView().background(.lightGray)
        view.addSubviews(mapView, btnCurrentLocation)
        
        activateConstraints(
            mapView.edges(),
            btnCurrentLocation.centerY().trailing(20)
        )
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Карта"
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
        
        self.mapView.addAnnotations(coordinates.map({ AutoAnnotation(coordinate: $0) }))

//        let apiClient = APIClientFicus()
//        apiClient.getIncluded(success: { [weak self] (model) in
//            guard let self = self else { return }
//            self.model = model
//            print(self.model ?? "error!")
//            }, failure: { (error) in
//                print(error.localizedDescription)
//        })
        
    }
}

extension MapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is AutoAnnotation {
            let autoAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: AutoAnnotationView.reuseID, for: annotation)
            return autoAnnotationView
        }
        return nil
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        mapView.removeOverlays(overlays)
        guard let userAnnotation = mapView.annotations.first(where: { $0 is MKUserLocation}),
            let autoAnnotation = view.annotation else { return }
        
        let request = MKDirections.Request()
        request.source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: userAnnotation.coordinate.latitude, longitude: userAnnotation.coordinate.longitude), addressDictionary: nil))
        request.destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: autoAnnotation.coordinate.latitude, longitude: autoAnnotation.coordinate.longitude), addressDictionary: nil))
        request.requestsAlternateRoutes = true
        request.transportType = .automobile
        
        let directions = MKDirections(request: request)
        
        directions.calculate { [unowned self] response, error in
            guard let unwrappedResponse = response else { return }
            
            for route in unwrappedResponse.routes {
                self.mapView.addOverlay(route.polyline)
                self.overlays.append(route.polyline)
                self.mapView.setVisibleMapRect(route.polyline.boundingMapRect, animated: true)
            }
            let autoDetailVC = AutoDetailViewController()
            autoDetailVC.willMove(toParent: self)
            self.addChild(autoDetailVC)
            self.view.addSubview(autoDetailVC.view)
            autoDetailVC.didMove(toParent: self)
            activateConstraints(
                autoDetailVC.view.height(300).width(of: self.view)
            )
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(polyline: overlay as! MKPolyline)
        renderer.strokeColor = UIColor.blue
        return renderer
    }
}

extension MapViewController: CLLocationManagerDelegate {
    
    
}
