//
//  ProfileViewController.swift
//  Ficus
//
//  Created by MacBook on 08/06/2019.
//  Copyright © 2019 Andrew. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    
    lazy var tableProfile: UITableView = {
        let table = UITableView()
        return table
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Профиль"
        self.view.background(.yellow)
    }
}
