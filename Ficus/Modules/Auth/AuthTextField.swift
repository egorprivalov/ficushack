//
//  AuthTextField.swift
//  Ficus
//
//  Created by Egor Privalov on 08.06.2019.
//  Copyright © 2019 Andrew. All rights reserved.
//

import UIKit

class AuthTextField: UIView {
    
    lazy var lblTitle = UILabel().lines(0).color(UIColor("#777777")).font(UIFont.systemFont(ofSize: 13, weight: .semibold)).alignment(.left)
    
    lazy var vBack: UIView = {
        let view = UIView().background(UIColor("#f7f7f7"))
//        view.translatesAutoresizingMaskIntoConstraints = false
//        view.layer.shadowColor = UIColor.black.cgColor
//        view.layer.shadowOffset = CGSize(width: 0, height: 10)
//        view.layer.shadowRadius = 10
//        view.layer.shadowOpacity = 0.05
        view.layer.cornerRadius = 15
        return view
    }()
    
    lazy var txtTitle = UITextField()
    
    convenience init(_ title: String,
                     placeholder: String,
                     returnKeyType: UIReturnKeyType = .default,
                     color: UIColor = UIColor("#777777")) {
        self.init()
        
        self.lblTitle.text = title
        self.txtTitle.placeholder = placeholder
        self.lblTitle.textColor = color
        self.txtTitle.returnKeyType = returnKeyType
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        vBack.addSubview(txtTitle)
        self.addSubviews(lblTitle, vBack)
        
        activateConstraints(
           lblTitle.dockTop(15),
           vBack.pinTop(10, to: lblTitle).leading(14).trailing(14).bottom(10).height(54),
           txtTitle.top(8).leading(14).trailing(14).bottom(8)
        )
        
    }
}
