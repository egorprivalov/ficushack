//
//  SignInViewController.swift
//  Ficus
//
//  Created by Egor Privalov on 08.06.2019.
//  Copyright © 2019 Andrew. All rights reserved.
//

import UIKit

class SignInViewController: UIViewController {
    
    private lazy var svContent = UIScrollView()
    private lazy var vContent = UIView()
    private lazy var btnSignUp = UIButton(target: self, selector: #selector(btnSignUp_Action), title: "Регистрация", color: UIColor.blue)
    
    private lazy var vLogin: AuthTextField = {
        let login = AuthTextField()
        login.lblTitle.text = "Логин"
        login.txtTitle.placeholder = "Логин"
        login.txtTitle.returnKeyType = .next
        return login
    }()
    
    private lazy var vPassword: AuthTextField = {
        let password = AuthTextField()
        password.lblTitle.text = "Пароль"
        password.txtTitle.placeholder = "Пароль"
        password.txtTitle.returnKeyType = .done
        return password
    }()
    
    private lazy var btnSignIn: UIButton = {
        let btn = UIButton().title("Вход").background(UIColor("#2CB8DD"))
        btn.setTitleColor(.white, for: .normal)
        btn.layer.cornerRadius = 15
        return btn
    }()
    
    override func loadView() {
        
        view = UIView().background(.white)
        vContent.addSubviews(vLogin, vPassword, btnSignIn, btnSignUp)
        svContent.addSubview(vContent)
        view.addSubview(svContent)
        
        activateConstraints(
            svContent.edges(),
            vContent.edges().width(of: svContent),
            vLogin.top(100).leading().trailing(),
            vPassword.pinTop(to: vLogin).leading().trailing(),
            btnSignIn.pinTop(30, to: vPassword).leading(100).trailing(100).height(54),
            btnSignUp.pinTop(20, to: btnSignIn).centerX().bottom(50)
        )
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Авторизация"
        self.hideKeyboard()
        setDelegates()
        addObservers()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func setDelegates() {
        vLogin.txtTitle.delegate = self
        vPassword.txtTitle.delegate = self
    }
    
    private func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 && vPassword.txtTitle.isEditing {
                self.view.frame.origin.y -= keyboardSize.height / 2
            }
        }
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    @objc private func btnSignIn_Action() {
        
        guard let login = vLogin.txtTitle.text, let password = vPassword.txtTitle.text else { return }
        
        if login.isEmpty {
            vLogin.vBack.shake()
            return
        }
        
        if password.isEmpty {
            vPassword.vBack.shake()
            return
        }
        
        // TODO: POST запрос на авторизацию.
    }
    
    @objc private func btnSignUp_Action() {
        let vc = SignUpViewController()
        let nav: UINavigationController = UINavigationController(rootViewController: vc)
        self.present(nav, animated: true, completion: nil)
    }
    
}

extension SignInViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == vLogin.txtTitle {
            vPassword.txtTitle.becomeFirstResponder()
        } else if textField == vPassword.txtTitle {
            textField.resignFirstResponder()
        }
        return false
    }
}

