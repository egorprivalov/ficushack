//
//  APIClientFicus.swift
//  manutdone
//
//  Created by MacBook on 02/04/2019.
//  Copyright © 2019 eprivalov. All rights reserved.
//

import Foundation

fileprivate enum API {
    static let getInclude = "Include/"

}

class APIClientFicus {
    
    private let baseURL: String = "http://193.124.58.65/comios/api/"
    private let apiClient: APIClient
    
    init() {
        apiClient = APIClient(baseURL: URL(string: baseURL)!)
    }
    /*
    func getPlayers(success: @escaping ([Player]) -> (), failure: @escaping (APIError) -> ()) {
        let request = APIRequest(method: .get, path: API.getPlayers)
        
        apiClient.perform(request) { (result) in
            // TODO: возможно стоит выносить на главный поток уже после декодирования данных
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    guard let response = try? response.decode(to: [Player].self) else {
                        failure(APIError.decodingFailure)
                        return
                    }
                    let players = response.body
                    success(players)
                case .failure(let error):
                    failure(error)
                }
            }
        }
    }
 */
    
    func getIncluded(success: @escaping (IncludeStructure) -> (), failure: @escaping (APIError) -> ()) {
        let request = APIRequest(method: .get, path: API.getInclude)
        
        apiClient.perform(request) { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    guard let response = try? response.decode(to: IncludeStructure.self) else {
                        failure(APIError.decodingFailure)
                        return
                    }
                    let model = response.body
                    success(model)
                case .failure(let error):
                    failure(error)
                }
            }
        }
    }

}
