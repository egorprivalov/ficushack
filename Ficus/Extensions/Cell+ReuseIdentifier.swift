//
//  Cell+ReuseIdentifier.swift
//  Cave
//
//  Created by Egor Privalov on 09/11/2018.
//  Copyright © 2018 eprivalov. All rights reserved.
//

import UIKit

extension UITableViewCell {
    static var reuseIdentifierCell: String{
        return String(describing: classForCoder())
    }
}

extension UICollectionReusableView {
    static var reuseIdentifierCell: String{
        return String(describing: classForCoder())
    }
}
