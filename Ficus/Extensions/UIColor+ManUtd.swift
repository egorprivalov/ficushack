//
//  UIColor+ManUtd.swift
//  manutdone
//
//  Created by Egor Privalov on 02/06/2019.
//  Copyright © 2019 eprivalov. All rights reserved.
//

import UIKit

extension UIColor {
    static let manUtdRed = UIColor(red: 201/255, green: 25/255, blue: 32/255, alpha: 1)
}
