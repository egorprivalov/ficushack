//
//  CALayer+Corners.swift
//  Cave
//
//  Created by Anton Pavlov on 16/11/2018.
//  Copyright © 2018 Anton Pavlov. All rights reserved.
//

import UIKit

extension CALayer {
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let maskPath = UIBezierPath(roundedRect: bounds,
                                    byRoundingCorners: corners,
                                    cornerRadii: CGSize(width: radius, height: radius))
        
        let shape = CAShapeLayer()
        shape.path = maskPath.cgPath
        mask = shape
    }
    
}
