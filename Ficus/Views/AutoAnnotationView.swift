//
//  AutoAnnotationView.swift
//  Ficus
//
//  Created by MacBook on 08/06/2019.
//  Copyright © 2019 Andrew. All rights reserved.
//

import UIKit
import MapKit

class AutoAnnotationView: MKAnnotationView {
    
    static var reuseID: String {
        return NSStringFromClass(self)
    }
    
    private lazy var imgCar = UIImageView(image: UIImage(named: "icon_car"))
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        
        self.addSubview(imgCar)
        
        activateConstraints(
            imgCar.center()
        )
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
