/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

*/

import Foundation
struct Coords : Codable {
	let lon : Double?
	let lat : Double?

	enum CodingKeys: String, CodingKey {
		case lon = "Lon"
		case lat = "Lat"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		lon = try values.decodeIfPresent(Double.self, forKey: .lon)
		lat = try values.decodeIfPresent(Double.self, forKey: .lat)
	}

}
